require "application_system_test_case"

class ResultsTest < ApplicationSystemTestCase
  setup do
    @result = results(:one)
  end

  test "visiting the index" do
    visit results_url
    assert_selector "h1", text: "Results"
  end

  test "creating a Result" do
    visit results_url
    click_on "New Result"

    fill_in "Division", with: @result.division
    fill_in "faculty_id", with: @result.faculty_id
    fill_in "Marks", with: @result.marks
    fill_in "Percentage", with: @result.percentage
    fill_in "semester_id", with: @result.semester_id
    fill_in "student_id", with: @result.student_id
    fill_in "Tutor", with: @result.tutor
    fill_in "Type", with: @result.type
    click_on "Create Result"

    assert_text "Result was successfully created"
    click_on "Back"
  end

  test "updating a Result" do
    visit results_url
    click_on "Edit", match: :first

    fill_in "Division", with: @result.division
    fill_in "faculty_id", with: @result.faculty_id
    fill_in "Marks", with: @result.marks
    fill_in "Percentage", with: @result.percentage
    fill_in "semester_id", with: @result.semester_id
    fill_in "student_id", with: @result.student_id
    fill_in "Tutor", with: @result.tutor
    fill_in "Type", with: @result.type
    click_on "Update Result"

    assert_text "Result was successfully updated"
    click_on "Back"
  end

  test "destroying a Result" do
    visit results_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Result was successfully destroyed"
  end
end
