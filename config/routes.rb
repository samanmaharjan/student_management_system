Rails.application.routes.draw do
  resources :semesters
  resources :results do
    collection do
      get :list_result
    end
  end
  resources :faculties
  resources :exams do
    collection do
      get :list_exam
    end
  end
  resources :books
  resources :students do
    collection do
      get :list_saman
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
