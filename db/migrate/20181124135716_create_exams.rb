class CreateExams < ActiveRecord::Migration[5.2]
  def change
    create_table :exams do |t|
      t.string :name
      t.integer :student_id
      t.string :faculty_id
      t.datetime :examination_date
      t.integer :semester_id
      t.string :category
      t.string :tutor

      t.timestamps
    end
  end
end
