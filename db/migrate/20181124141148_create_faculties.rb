class CreateFaculties < ActiveRecord::Migration[5.2]
  def change
    create_table :faculties do |t|
      t.string :name
      t.integer :student_id
      t.string :faculty_id
      t.datetime :start_date
      t.integer :semester_id
      t.datetime :end_date
      t.string :tutor

      t.timestamps
    end
  end
end
