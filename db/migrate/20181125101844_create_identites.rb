class CreateIdentites < ActiveRecord::Migration[5.2]
  def change
    create_table :identites do |t|
      t.string :symbol_no
      t.string :batch
    end
  end
end
