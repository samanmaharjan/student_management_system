class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.integer :isbn
      t.string :category
      t.string :publication
      t.datetime :publication_date
      t.integer :faculty_id
      t.string :author
      t.float :edition

      t.timestamps
    end
  end
end
