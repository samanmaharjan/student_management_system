class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.integer :student_id
      t.string :gender
      t.integer :age
      t.integer :phone
      t.integer :faculty_id
      t.integer :semester_id
      t.string :guardian_name
      t.integer :guardian_phone

      t.timestamps
    end
  end
end
