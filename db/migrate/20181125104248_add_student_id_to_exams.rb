class AddStudentIdToExams < ActiveRecord::Migration[5.2]
  def change
    add_column :exams, :student_ids, :integer
  end
end
