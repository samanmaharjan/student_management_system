class CreateSemesters < ActiveRecord::Migration[5.2]
  def change
    create_table :semesters do |t|
      t.integer :student_id
      t.string :tutor
      t.integer :faculty_id
      t.integer :semester_id

      t.timestamps
    end
  end
end
