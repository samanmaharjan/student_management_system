class CreateResults < ActiveRecord::Migration[5.2]
  def change
    create_table :results do |t|
      t.integer :student_id
      t.string :tutor
      t.integer :faculty_id
      t.float :marks
      t.string :division
      t.float :percentage
      t.string :category
      t.integer :semester_id

      t.timestamps
    end
  end
end
