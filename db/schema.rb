# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_25_104248) do

  create_table "books", force: :cascade do |t|
    t.string "name"
    t.integer "isbn"
    t.string "category"
    t.string "publication"
    t.datetime "publication_date"
    t.integer "faculty_id"
    t.string "author"
    t.float "edition"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "student_id"
  end

  create_table "exams", force: :cascade do |t|
    t.string "name"
    t.integer "student_id"
    t.string "faculty_id"
    t.datetime "examination_date"
    t.integer "semester_id"
    t.string "category"
    t.string "tutor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "student_ids"
  end

  create_table "faculties", force: :cascade do |t|
    t.string "name"
    t.integer "student_id"
    t.string "faculty_id"
    t.datetime "start_date"
    t.integer "semester_id"
    t.datetime "end_date"
    t.string "tutor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identites", force: :cascade do |t|
    t.string "symbol_no"
    t.string "batch"
  end

  create_table "results", force: :cascade do |t|
    t.integer "student_id"
    t.string "tutor"
    t.integer "faculty_id"
    t.float "marks"
    t.string "division"
    t.float "percentage"
    t.string "category"
    t.integer "semester_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "semesters", force: :cascade do |t|
    t.integer "student_id"
    t.string "tutor"
    t.integer "faculty_id"
    t.integer "semester_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "student_id"
    t.string "gender"
    t.integer "age"
    t.integer "phone"
    t.integer "faculty_id"
    t.integer "semester_id"
    t.string "guardian_name"
    t.integer "guardian_phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
