json.extract! result, :id, :student_id, :tutor, :faculty_id, :marks, :division, :percentage, :category, :semester_id, :created_at, :updated_at
json.url result_url(result, format: :json)
