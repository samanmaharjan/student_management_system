json.extract! faculty, :id, :name, :student_id, :faculty_id, :start_date, :semester_id, :end_date, :tutor, :created_at, :updated_at
json.url faculty_url(faculty, format: :json)
