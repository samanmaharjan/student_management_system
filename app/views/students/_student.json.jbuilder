json.extract! student, :name, :student_id, :gender, :age, :phone, :faculty_id, :semester_id, :guardian_name, :guardian_phone, :created_at, :updated_at
json.url student_url(student, format: :json)
