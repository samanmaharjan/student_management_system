json.extract! semester, :student_id, :tutor, :faculty_id, :semester_id, :created_at, :updated_at
json.url semester_url(semester, format: :json)
