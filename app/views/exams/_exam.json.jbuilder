json.extract! exam, :id, :name, :student_id, :faculty_id, :examination_date, :semester_id, :category, :tutor, :created_at, :updated_at
json.url exam_url(exam, format: :json)
