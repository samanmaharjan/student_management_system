json.extract! book, :id, :name, :isbn, :category, :publication, :publication_date, :faculty_id, :author, :edition, :created_at, :updated_at
json.url book_url(book, format: :json)
